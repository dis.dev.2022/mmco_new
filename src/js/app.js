"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import Dropdown from "./components/dropdown.js";
import Spoilers from "./components/spoilers.js";
import collapse from './components/collapse.js'; // Сворачиваемые блоки
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery
import Notify from 'simple-notify'
import {WebpMachine} from "webp-hero"

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// IE Webp Support
const webpMachine = new WebpMachine();
webpMachine.polyfillDocument();


// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// Меню для мобильной версии
mobileNav();

// Spoilers
Spoilers();

// Сворачиваемые блоки
collapse();

// Dropdown
Dropdown();

// Sliders
import "./components/sliders.js";



// Modal Fancybox
Fancybox.bind('[data-fancybox]', {
    autoFocus: false
});

document.addEventListener('scroll', (event) => {
    const header = document.querySelector('[data-header]')
    const sticky = document.querySelector('.header-sticky')

    if (sticky.getBoundingClientRect().top < 0) {
        header.classList.add("header--fixed");
    } else {
        header.classList.remove("header--fixed");
    }
})

if (document.querySelector('.primary__buttons')) {

    document.addEventListener('scroll', (event) => {
        const header = document.querySelector('[data-header]')
        const buttons = document.querySelector('.primary__buttons')

          if (buttons.getBoundingClientRect().top < 0) {
            header.classList.add("header--buttons");
        } else {
            header.classList.remove("header--buttons");
        }
    })
}

// Add favorite
document.addEventListener('click', (event) => {
    if(event.target.closest('[data-add-favorite]')) {
        event.target.closest('[data-add-favorite]').classList.toggle('added')

        new Notify ({
            status: 'success',
            title: '',
            text: 'Событие успешно добавлено  в избранное, вы можете найти его в личном кабинете.',
            effect: 'slide',
            speed: 300,
            customClass: '',
            customIcon: '',
            showIcon: false,
            showCloseButton: true,
            autoclose: true,
            autotimeout: 3000,
            gap: 20,
            distance: 20,
            type: 1,
            position: 'right top'
        })
    }
})

// Notify
document.addEventListener('click', (event) => {
    if(event.target.closest('[data-notify]')) {
        let notifyType = event.target.closest('[data-notify]').dataset.notify
        let notifyText = event.target.closest('[data-notify]').dataset.notifyText

        new Notify ({
            status: notifyType,
            title: '',
            text: notifyText,
            effect: 'slide',
            speed: 300,
            customClass: '',
            customIcon: '',
            showIcon: false,
            showCloseButton: true,
            autoclose: true,
            autotimeout: 3000,
            gap: 20,
            distance: 20,
            type: 1,
            position: 'right top'
        })
    }
})

// Counters
let Counters = () => {
    const counters = document.querySelectorAll('[data-counter]')
    if (counters.length > 0) {
        counters.forEach(elem => {
            let counterNum =  parseInt(elem.dataset.counter)
            let counterStep =  parseInt(elem.dataset.step)
            let counterTime =  parseInt(elem.dataset.time)

            let n = 0;
            let t = Math.round(counterTime / (counterNum / counterStep));


            let interval = setInterval(() => {
                n = n + counterStep;
                if (n === counterNum) {
                    clearInterval(interval);
                }
                elem.innerHTML = usefulFunctions.getDigFormat(n);

            }, t);
        });
    }
}


// Sorter
if(document.querySelector('[data-sorter]')) {

    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-sorter-toggle]')) {
            event.target.closest('[data-sorter]').classList.toggle('open')
        }
        else {
            document.querySelectorAll('[data-sorter]').forEach(elem => {
                elem.classList.remove('open');
            });
        }
    })

    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-sorter-button]')) {
            const sorter = event.target.closest('[data-sorter]')
            const sorterItem = event.target.closest('[data-sorter-button]')

            document.querySelectorAll('[data-sorter-button]').forEach(elem => {
                elem.classList.remove('active');
            });
            sorterItem.classList.add('active')
        }
    })
}

// Tags
document.addEventListener('click', (event) => {
    if(event.target.closest('[data-tag]')) {
        const tagItem = event.target.closest('[data-tag]')
        const tagContainer = event.target.closest('[data-tags]')
        const tagArray = tagContainer.querySelectorAll('[data-tag]')
        let isSelected = tagItem.classList.contains('selected')

        if(!isSelected) {
            tagArray.forEach(elem => {
                elem.classList.remove('selected');
            });
            tagItem.classList.add('selected')
        }
    }
})

// ----------------------------
// функция определяет нахождение элемента в области видимости
// если элемент видно - возвращает true
// если элемент невидно - возвращает false
function isOnVisibleSpace(element) {
    var bodyHeight = window.innerHeight;
    var elemRect = element.getBoundingClientRect();
    var offset   = elemRect.top;// - bodyRect.top;
    if(offset<0) return false;
    if(offset>bodyHeight) return false;
    return true;
}

// глобальный объект с элементами, для которых отслеживаем их положение в зоне видимости
let listenedElements = [];
// обработчик события прокрутки экрана. Проверяет все элементы добавленные в listenedElements
// на предмет попадания(выпадения) в зону видимости
window.onscroll = function() {
    listenedElements.forEach(item=>{
        // проверяем находится ли элемент в зоне видимости
        let result = isOnVisibleSpace(item.el);

        // если элемент находился в зоне видимости и вышел из нее
        // вызываем обработчик выпадения из зоны видимости
        if(item.el.isOnVisibleSpace && !result){
            item.el.isOnVisibleSpace = false;
            item.outVisibleSpace(item.el);
            return;
        }
        // если элемент находился вне зоны видимости и вошел в нее
        // вызываем обработчик попадания в зону видимости
        if(!item.el.isOnVisibleSpace && result){
            item.el.isOnVisibleSpace = true;
            item.inVisibleSpace(item.el);
            return;
        }
    });
}

// функция устанавливает обработчики событий
// появления элемента в зоне видимости и
// выхода из нее

function onVisibleSpaceListener(elementId, cbIn, cbOut) {
    // получаем ссылку на объект элемента
    let el = elementId;
    // добавляем элемент и обработчики событий
    // в массив отслеживаемых элементов
    listenedElements.push({
        el: el,
        inVisibleSpace: cbIn,
        outVisibleSpace: cbOut
    });
}

const charts = document.querySelectorAll('[data-chart]')
if (charts.length > 0) {
    charts.forEach(elem => {
        onVisibleSpaceListener(elem,
            el=>{
                // функция вызываемая при попадании элемента в зону видимости
                // тут вставляем код запуска анимации

                const count = elem.querySelector('[data-counter]')

                console.log(count);
                if (count) {
                    console.log('count')
                    let counterNum =  parseInt(count.dataset.counter)
                    let counterStep =  parseInt(count.dataset.step)
                    let counterTime =  parseInt(count.dataset.time)

                    let n = 0;
                    let t = Math.round(counterTime / (counterNum / counterStep));


                    let interval = setInterval(() => {
                        n = n + counterStep;
                        if (n > counterNum) {
                            clearInterval(interval);
                            n = counterNum
                        }
                        count.innerHTML = usefulFunctions.getDigFormat(n);

                    }, t);
                }
                el.classList.remove('disable')

            },
            el=>{
                // функция вызываемая при выпадении элемента из зоны видимости
                // тут вставляем код остановки анимации
                el.classList.add('disable')
            }
        );
    });
}

// устанавливаем обработчики для элемента "video"

//-----------------------------

// Add favorite 2
document.addEventListener('click', (event) => {
    if(event.target.closest('[data-favorite-add]')) {
        event.target.closest('[data-favorite-add]').classList.toggle('added')
    }
})



window.addEventListener("load", function (e) {

    if(document.querySelector('[data-counters]')) {
        Counters()
    }
});
