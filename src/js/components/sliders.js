/*
Документация по работе в шаблоне:
Документация слайдера: https://swiperjs.com/
Сниппет(HTML): swiper
*/

import Swiper, {Autoplay, Navigation, Pagination, Thumbs} from 'swiper';

// Инициализация слайдеров
function initSliders() {
    // Перечень слайдеров

    if(document.querySelector('[data-events]')) {
        new Swiper('[data-events]', {
            modules: [Navigation],
            init: true,
            loop: false,
            observer: true,
            observeParents: true,
            spaceBetween: 15,
            slidesPerView: 'auto',
            navigation: {
                nextEl: '[data-events-next]',
                prevEl: '[data-events-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 24,
                },
            },
        });
    }

    if (document.querySelector('[data-community-media]')) {
        new Swiper('[data-community-media]', {
            modules: [Navigation],
            init: true,
            loop: false,
            observer: true,
            observeParents: true,
            spaceBetween: 12,
            slidesPerView: 'auto',
            navigation: {
                nextEl: '[data-community-next]',
                prevEl: '[data-community-prev]',
            },
            on: {
                init: function () {
                    document.querySelector('[data-community-slides]').innerHTML = this.slides.length
                },
                slideChange: function () {
                    document.querySelector('[data-community-active]').innerHTML = (this.realIndex + 1)
                }
            }
        });
    }

    if(document.querySelector('[data-news]')) {
        new Swiper('[data-news]', {
            modules: [Navigation],
            init: true,
            loop: false,
            observer: true,
            observeParents: true,
            spaceBetween: 12,
            slidesPerView: 'auto',
            navigation: {
                nextEl: '[data-news-next]',
                prevEl: '[data-news-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 24,
                },
            },
        });
    }

    if(document.querySelector('[data-top]')) {
        new Swiper('[data-top]', {
            modules: [Navigation],
            init: true,
            loop: false,
            observer: true,
            observeParents: true,
            spaceBetween: 12,
            slidesPerView: 'auto',
            navigation: {
                nextEl: '[data-top-next]',
                prevEl: '[data-top-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 24,
                },
            },
        });
    }

    if(document.querySelector('[data-partners]')) {

        const partnerArray = document.querySelectorAll('[data-partner-num]');

        const partnersLogos = new Swiper('[data-partners-logos]', {
            modules: [Autoplay],
            init: true,
            loop: true,
            grabCursor: true,
            mousewheelControl: true,
            keyboardControl: true,
            initialSlide: 3,
            spaceBetween: 0,
            speed: 3000,
            autoplay: {
                delay: 0,
                disableOnInteraction: false
            },
            freeMode: {
                enabled: true,
            },
            slidesPerView: 'auto',
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 24,
                },
            },
        });

        const partnersComments = new Swiper('[data-partners-comments]', {
            modules: [Navigation],
            init: true,
            loop: false,
            observer: true,
            observeParents: true,
            initialSlide: 3,
            spaceBetween: 12,
            slidesPerView: 1,
            breakpoints: {
                768: {
                    spaceBetween: 30,
                },
            },
            navigation: {
                nextEl: '[data-partners-next]',
                prevEl: '[data-partners-prev]',
            },
            on: {
                slideChange: function () {
                    partnerArray.forEach(elem => {
                        elem.classList.remove('active');
                    });
                    document.querySelector(`[data-partner-num="${this.realIndex}"]`).classList.add('active');
                },
            }
        });


        document.addEventListener('click', (event) => {
            if (event.target.closest('[data-partner-num]')) {

                let currentPartner = event.target.closest('[data-partner-num]')
                let currentPartnerNum = parseInt(currentPartner.dataset.partnerNum)

                partnerArray.forEach(elem => {
                    elem.classList.remove('active');
                });
                currentPartner.classList.add('active')
                partnersComments.slideTo(currentPartnerNum, 600)
            }
        })
    }

    if(document.querySelector('[data-lib]')) {
        new Swiper('[data-lib]', {
            modules: [Navigation],
            init: true,
            loop: false,
            observer: true,
            observeParents: true,
            spaceBetween: 15,
            slidesPerView: 'auto',
            navigation: {
                nextEl: '[data-lib-next]',
                prevEl: '[data-lib-prev]',
            },
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 24,
                },
            },
        });
    }

    if (document.querySelector('[data-upcoming]')) {
        let upcomingQuerySize  = 1280;
        let upcomingSlider = null;

        function upcomingSliderInit() {
            if(!upcomingSlider) {
                upcomingSlider = new Swiper('[data-upcoming]', {
                    observer: true,
                    observeParents: true,
                    slidesPerView: 'auto',
                    spaceBetween: 15,
                    breakpoints: {
                        768: {
                            slidesPerView: 'auto',
                            spaceBetween: 24,
                        },
                    },
                });
            }
        }

        function upcomingSliderDestroy() {
            if(upcomingSlider) {
                upcomingSlider.destroy();
                upcomingSlider = null;
            }
        }

        if (document.documentElement.clientWidth < upcomingQuerySize) {
            upcomingSliderInit()
        }

        window.addEventListener('resize', function (){

            if (document.documentElement.clientWidth < upcomingQuerySize) {
                upcomingSliderInit()
            }
            else {
                upcomingSliderDestroy()
            }
        });
    }

    if(document.querySelector('[data-sorter]')) {
        new Swiper('[data-sorter]', {
            init: true,
            loop: false,
            freeMode: true,
            spaceBetween: 10,
            slidesPerView: 'auto',
        });
    }

    // tags slider
    if (document.querySelector('[data-tags]')) {
        let tagsQuerySize  = 1280;
        let tagsSlider = null;

        function tagsSliderInit() {
            if(!tagsSlider) {
                tagsSlider = new Swiper('[data-tags]', {
                    observer: true,
                    observeParents: true,
                    slidesPerView: 'auto',
                    spaceBetween: 10,
                });
            }
        }

        function tagsSliderDestroy() {
            if(tagsSlider) {
                tagsSlider.destroy();
                tagsSlider = null;
            }
        }

        if (document.documentElement.clientWidth < tagsQuerySize) {
            tagsSliderInit()
        }

        window.addEventListener('resize', function (){

            if (document.documentElement.clientWidth < tagsQuerySize) {
                tagsSliderInit()
            }
            else {
                tagsSliderDestroy()
            }
        });
    }

    // Expert slider
    if (document.querySelector('[data-experts]')) {
        let expertQuerySize  = 1280;
        let expertSlider = null;

        function expertSliderInit() {
            if(!expertSlider) {
                expertSlider = new Swiper('[data-experts]', {
                    observer: true,
                    observeParents: true,
                    slidesPerView: 'auto',
                    spaceBetween: 10,
                    breakpoints: {
                        768: {
                            slidesPerView: 'auto',
                            spaceBetween: 20,
                        },
                    },
                });
            }
        }

        function expertSliderDestroy() {
            if(expertSlider) {
                expertSlider.destroy();
                expertSlider = null;
            }
        }

        if (document.documentElement.clientWidth < expertQuerySize) {
            expertSliderInit()
        }

        window.addEventListener('resize', function (){

            if (document.documentElement.clientWidth < expertQuerySize) {
                expertSliderInit()
            }
            else {
                expertSliderDestroy()
            }
        });
    }

    // Action slider
    if (document.querySelector('[data-action]')) {
        let actionQuerySize  = 1280;
        let actionSlider = null;

        function actionSliderInit() {
            if(!actionSlider) {
                actionSlider = new Swiper('[data-action]', {
                    observer: true,
                    observeParents: true,
                    slidesPerView: 'auto',
                    spaceBetween: 10,
                    breakpoints: {
                        768: {
                            slidesPerView: 'auto',
                            spaceBetween: 20,
                        },
                    },
                });
            }
        }

        function actionSliderDestroy() {
            if(actionSlider) {
                actionSlider.destroy();
                actionSlider = null;
            }
        }

        if (document.documentElement.clientWidth < actionQuerySize) {
            actionSliderInit()
        }

        window.addEventListener('resize', function (){

            if (document.documentElement.clientWidth < actionQuerySize) {
                actionSliderInit()
            }
            else {
                actionSliderDestroy()
            }
        });
    }

    if (document.querySelector('[data-action-slider]')) {
        new Swiper('[data-action-slider]', {
            observer: true,
            observeParents: true,
            slidesPerView: 'auto',
            spaceBetween: 10,
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 24,
                },
            },
        });
    }

    if(document.querySelector('[data-line]')) {
        new Swiper('[data-line]', {
            init: true,
            loop: false,
            observer: true,
            observeParents: true,
            spaceBetween: 10,
            slidesPerView: 'auto',
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 24,
                },
            },
        });
    }

    if (document.querySelector('[data-logotype]')) {

        new Swiper('[data-logotype]', {
            modules: [Autoplay],
            loop: true,
            spaceBetween: 0,
            speed: 2000,
            grabCursor: true,
            mousewheelControl: true,
            keyboardControl: true,
            autoplay: {
                delay: 0,
                disableOnInteraction: false
            },
            freeMode: {
                enabled: true,
            },
            slidesPerView: 'auto',
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 16,
                },
            },
        });
    }

    if (document.querySelector('[data-interest]')) {

        new Swiper('[data-interest]', {
            init: true,
            loop: false,
            observer: true,
            observeParents: true,
            spaceBetween: 10,
            slidesPerView: 'auto',
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 24,
                },
            },
        });
    }

    if (document.querySelector('[data-speakers]')) {

        new Swiper('[data-speakers]', {
            init: true,
            loop: false,
            observer: true,
            observeParents: true,
            spaceBetween: 16,
            slidesPerView: 'auto',
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 24,
                },
            },
        });
    }

    if (document.querySelector('[data-trailer]')) {

        new Swiper('[data-trailer]', {
            modules: [Autoplay],
            init: true,
            loop: true,
            observer: true,
            observeParents: true,
            spaceBetween: 14,
            slidesPerView: 'auto',
            speed: 6000,
            autoplay: {
                delay: 0,
                disableOnInteraction: false,
            },
        });

        new Swiper('[data-trailer-reverse]', {
            modules: [Autoplay],
            init: true,
            loop: true,
            observer: true,
            observeParents: true,
            spaceBetween: 14,
            slidesPerView: 'auto',
            speed: 6000,
            autoplay: {
                delay: 0,
                disableOnInteraction: false,
                reverseDirection: true
            },
        });
    }

    if (document.querySelector('[data-offer-block]')) {
        new Swiper('[data-offer-block]', {
            modules: [Autoplay],
            init: true,
            loop: true,
            observer: true,
            observeParents: true,
            spaceBetween: 20,
            slidesPerView: 1,
            initialSlide: 1,
            speed: 6000,
            autoplay: {
                delay: 0,
                disableOnInteraction: true,
            },
        });
    }

}

window.addEventListener("load", function (e) {
    // Запуск инициализации слайдеров
    initSliders();
});
